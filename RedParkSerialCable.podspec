Pod::Spec.new do |s|
	s.name					= 'RedParkSerialCable'
	s.summary				= 'A way to connect using the redpark serial cable.'
	s.homepage				= 'git@bitbucket.org:boland/redparkserialcable.git'
	s.license				= 'MIT'
	s.source				= { :git => 'https://bitbucket.org/boland/redparkserialcable.git'}
	s.source_files			= 'inc/*.{h,m}'
	s.resources				= 'lib/*'
	s.platform				= :ios, '7.0'	
	s.requires_arc			= true
end